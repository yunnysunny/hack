<?php
/**
 *  941book
 *  A sample c2c site code , which can be used as beginner's course.
 *
 *  Copyright (c) 2011 phpgroup
 *  Licensed under the MIT licenses:
 *  http://www.opensource.org/licenses/mit-license.php
 *
 * @email phpgroup5.com
 * @version 0.2
 *
 */
//globaldb.php
include_once 'dbconnect.php';
include_once 'config.php';
global $db;
$db=new dbclass();
$db->dbConnect($dbServer,$dbUser,$dbPass);
$db->dbSelect($dbName);
?>
